/*
StyledBasketWrapper - section
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-between;
  margin: 2rem 2rem 0;
  width: calc(100% - 4rem);


StyledToyColumn - article
  width: 48%;


StyledToyWrapper - button
  align-items: center;
  background: none;
  border-color: ** white **
  border-style: solid;
  border-width: 1px;
  color: ** white **
  cursor: pointer;
  display: flex;
  flex-wrap: wrap;
  font-size: 1rem;
  justify-content: space-between;
  margin: 0 0 2rem;
  padding: 1rem;
  width: 100%;


StyledToyText - span
  color: ** white **
  font-size: 1rem;
  &::first-letter {
    text-transform: uppercase;
  }


StyledPrice - article
  border-color: ** white **
  border-style: solid;
  border-width: 1px;
  color: ** white **
  font-size: 1.5rem;
  padding: 2rem;
  text-align: right;

*/