/*
AuthBackground - aside
  align-items: center;
  background: ** opaque **
  display: flex;
  height: 100%;
  justify-content: center;
  left: 0;
  position: fixed;
  top: 0;
  width: 100%;


AuthBox - section
  border: 1px solid ** white **
  background: ** black **
  padding: 1rem 3rem;
  text-align: center;
  width: 20rem;


AuthTitle - h2
  color: ** white **
  font-size: 1rem;
  padding: 0 0 1rem;
  text-align: center;
  text-transform: uppercase;


AuthInput - input
  background: transparent !important;
  border-style: solid;
  border-color: ** red ** ou ** white **
  border-width: 0 0 1px;
  color: ** red ** ou ** white ** !important;
  display: block;
  font-size: 1rem;
  margin-bottom: 2rem;
  padding: 0 0 0.5rem;
  outline: none;
  text-align: left;
  width: 100%;
  &:last-of-type {
    margin: 0;
  }


AuthMessage - p
  color: ** red **
  opacity: ** 1 ou 0 **
  font-weight: bold;
  padding: 0.7rem 0;


AuthButton - button
  background-color: transparent;
  border: 1px solid ** white **
  color: ** white **
  cursor: pointer;
  font-size: 1rem;
  outline: none;
  padding: 0.5rem 2rem;
  text-align: center;
  &:hover {
    opacity: 0.5;
  }

*/